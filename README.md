# README #

Public Transport Departure times API project.

### Purpose ###

* This is the server side component of the project.
* The [pt-web](https://bitbucket.org/benupsavs/pt-web) project is the client side.

### Setup ###

* Install Maven or NetBeans
* Run **mvn clean package -DskipTests=true** - The runnable application will be in target/pt-api.jar
* Maven will download the dependencies, with the main two being Grizzly and EclipseLink MOXy.
* If the project is loaded in NetBeans, click **Clean and Build**.
* A default in-memory persistence engine will be used for caching the NextBus data
* Run tests with mvn surefire:test
* Run the API with java -jar target/pt-api.jar [endpoint URI]
* If endpoint URI is not specified, it defaults to http://0.0.0.0:8080/pt/ which will cause it to listen on all network interfaces.
* Run the client web application against the server.

### Architecture ###

* Java frameworks such as Spring tend to be very heavyweight. For this reason, This project uses features from core Java wherever possible. The main two libraries are:
	* Grizzly, which provides an efficient RESTful server using asynchronous I/O.
	* EclipseLink MOXy which integrates with Grizzly and converts between JSON, XML and memory data models.
* As to not overly complicate things, the data model, API services, persistence engine etc are all in the same project. These would normally be split into separate Maven projects in a larger application.
* The persistence engine (which is the *PersistenceEngine* interface in the code) is discovered using the Java ServiceLoader interface, allowing a new persistence engine to be installed without recompiling, by adding the custom persistence engine jar to the Java classpath. The current fallback is to use an in-memory persistence engine.
* The EclipseLink MOXy library is used for translation between NextBus's XML and the API service's JSON, using the same data models.

### Scalability Concerns ###
* In larger installations, the API servers would be fronted by load balancers to scale the service. The main concern with scalability is the persistence engine. The current in-memory fallback persistence engine will cache its data on each node, and eventually as the number of nodes increases, the number of requests to NextBus may increase beyond a reasonable amount. The workaround for this would be to write a persistence engine implementation that saved the data to an elastic database such as CouchBase, giving all nodes access to the same data store and reducing the number of requests to NextBus.