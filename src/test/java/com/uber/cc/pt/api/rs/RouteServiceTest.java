package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.Main;
import com.uber.cc.pt.api.client.BaseTransitClient;
import com.uber.cc.pt.api.client.TestTransitClient;
import com.uber.cc.pt.api.model.Route;
import com.uber.cc.pt.api.model.Stop;
import java.util.List;
import javax.ws.rs.core.GenericType;
import static org.junit.Assert.*;
import org.junit.Test;

public class RouteServiceTest extends ApiTest {

    @Test
    public void testGetRoutesForAgency() {
        System.out.println("GET route/sf-muni");

        List<Route> agencies = target.path("route/sf-muni").request().get(new GenericType<List<Route>>() {});
        assertEquals(82, agencies.size());
    }

    @Test
    public void testGetClosestStops() {
        System.out.println("GET route/closest/37.780871,-122.495285");

        assertTrue(BaseTransitClient.getTransitClient().getClass() == TestTransitClient.class);
        Main.prime();
        List<Stop> stops = target.path("route/closest/37.780871,-122.495285").request().get(new GenericType<List<Stop>>() {});
        assertEquals(5, stops.size());
    }
}
