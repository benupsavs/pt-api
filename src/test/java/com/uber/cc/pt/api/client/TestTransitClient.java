package com.uber.cc.pt.api.client;

import com.uber.cc.pt.api.model.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of transport client for testing purposes. This client
 * is not really a client - it returns static test data.
 */
public class TestTransitClient implements TransitClient {

    @Override
    public List<Agency> getAgencies() throws IOException {
        final Agency[] agencies = {
            new Agency("actransit", "AC Transit", "California-Northern"),
            new Agency("jhu-apl", "APL", "Maryland"),
            new Agency("art", "Asheville Redefines Transit", "North Carolina"),
            new Agency("camarillo", "Camarillo Area (CAT)", "Camarillo (CAT)", "California-Southern"),
            new Agency("ccrta", "Cape Cod Regional Transit Authority", "CCRTA", "Massachusetts"),
            new Agency("chapel-hill", "Chapel Hill Transit", "Chapel Hill", "North Carolina"),
            new Agency("charles-river", "Charles River TMA - EZRide", "Charles River EZRide", "Massachusetts"),
            new Agency("charm-city", "Charm City Circulator", "Maryland"),
            new Agency("ccny", "City College NYC", "New York"),
            new Agency("oxford-ms", "City of Oxford", "Mississippi"),
            new Agency("collegetown", "Collegetown Shuttle", "Maryland"),
            new Agency("cyride", "CyRide", "Iowa"),
            new Agency("dc-circulator", "DC Circulator", "District of Columbia"),
            new Agency("da", "Downtown Connection", "New York"),
            new Agency("dumbarton", "Dumbarton Express", "Dumbarton Exp", "California-Northern"),
            new Agency("ecu", "East Carolina University", "North Carolina"),
            new Agency("emery", "Emery-Go-Round", "California-Northern"),
            new Agency("fairfax", "Fairfax (CUE)", "Virginia"),
            new Agency("foothill", "Foothill Transit", "California-Southern"),
            new Agency("ft-worth", "Fort Worth The T", "Texas"),
            new Agency("gmu", "George Mason University", "Virginia"),
            new Agency("georgia-college", "Georgia College", "Georgia"),
            new Agency("glendale", "Glendale Beeline", "California-Southern"),
            new Agency("south-coast", "Gold Coast Transit", "California-Southern"),
            new Agency("indianapolis-air", "Indianapolis International Airport", "Indiana"),
            new Agency("jtafla", "Jacksonville Transportation Authority", "Jacksonville", "Florida"),
            new Agency("lasell", "Lasell College", "Massachusetts"),
            new Agency("lametro", "Los Angeles Metro", "California-Southern"),
            new Agency("lametro-rail", "Los Angeles Rail", "California-Southern"),
            new Agency("mbta", "MBTA", "Massachusetts"),
            new Agency("mit", "Massachusetts Institute of Technology", "MIT", "Massachusetts"),
            new Agency("sf-mission-bay", "Mission Bay", "California-Northern"),
            new Agency("moorpark", "Moorpark Transit", "California-Southern"),
            new Agency("bronx", "NYC MTA - Bronx", "New York"),
            new Agency("brooklyn", "NYC MTA - Brooklyn", "New York"),
            new Agency("staten-island", "NYC MTA - Staten Island", "New York"),
            new Agency("nctd", "North County Transit District", "NCTD", "California-Southern"),
            new Agency("nova-se", "Nova Southeastern University", "Nova", "Florida"),
            new Agency("omnitrans", "Omnitrans", "California-Southern"),
            new Agency("pvpta", "Palos Verdes Transit", "California-Southern"),
            new Agency("sria", "Pensacola Beach (SRIA)", "SRIA", "Florida"),
            new Agency("portland-sc", "Portland Streetcar", "Oregon"),
            new Agency("pgc", "Prince Georges County", "Maryland"),
            new Agency("reno", "RTC RIDE, Reno", "Nevada"),
            new Agency("radford", "Radford Transit", "Virginia"),
            new Agency("howard", "Regional Transportation Agency of Central Maryland", "Maryland"),
            new Agency("roosevelt", "Roosevelt Island", "New York"),
            new Agency("rutgers-newark", "Rutgers Univ. Newark College Town Shuttle", "Rutgers Newark Shuttle", "New Jersey"),
            new Agency("rutgers", "Rutgers University", "Rutgers", "New Jersey"),
            new Agency("sf-muni", "San Francisco Muni", "SF Muni", "California-Northern"),
            new Agency("seattle-sc", "Seattle Streetcar", "Washington"),
            new Agency("simi-valley", "Simi Valley (SVT)", "California-Southern"),
            new Agency("stl", "Societe de transport de Laval", "Laval", "Quebec"),
            new Agency("thousand-oaks", "Thousand Oaks Transit (TOT)", "Thousand Oaks Transit", "California-Southern"),
            new Agency("thunderbay", "Thunder Bay", "Ontario"),
            new Agency("ttc", "Toronto Transit Commission", "Toronto TTC", "Ontario"),
            new Agency("unitrans", "Unitrans ASUCD/City of Davis", "Unitrans", "California-Northern"),
            new Agency("ucsf", "University of California San Francisco", "California-Northern"),
            new Agency("umd", "University of Maryland", "Maryland"),
            new Agency("umn-twin", "University of Minnesota", "Minnesota"),
            new Agency("vista", "Ventura Intercity (VISTA)", "Ventura (VISTA)", "California-Southern"),
            new Agency("wku", "Western Kentucky University", "Kentucky"),
            new Agency("york-pa", "York College", "Pennsylvania"),
        };

        return Arrays.asList(agencies);
    }

    @Override
    public List<Route> getRoutesForAgency(String agency) throws IOException {
        Route[] routes;
        switch (agency) {
            case "sf-muni":
                routes = new Route[] {
                    new Route("F", "F-Market & Wharves"),
                    new Route("J", "J-Church"),
                    new Route("KT", "KT-Ingleside/Third Street"),
                    new Route("L", "L-Taraval"),
                    new Route("M", "M-Ocean View"),
                    new Route("N", "N-Judah"),
                    new Route("NX", "NX-Express"),
                    new Route("1", "1-California"),
                    new Route("1AX", "1AX-California A Express"),
                    new Route("1BX", "1BX-California B Express"),
                    new Route("2", "2-Clement"),
                    new Route("3", "3-Jackson"),
                    new Route("5", "5-Fulton"),
                    new Route("5R", "5R-Fulton Rapid"),
                    new Route("6", "6-Haight-Parnassus"),
                    new Route("7", "7-Haight-Noriega"),
                    new Route("7R", "7R-Haight-Noriega Rapid"),
                    new Route("7X", "7X-Noriega Express"),
                    new Route("8", "8-Bayshore"),
                    new Route("8AX", "8AX-Bayshore A Express"),
                    new Route("8BX", "8BX-Bayshore B Express"),
                    new Route("9", "9-San Bruno"),
                    new Route("9R", "9R-San Bruno Rapid"),
                    new Route("10", "10-Townsend"),
                    new Route("12", "12-Folsom-Pacific"),
                    new Route("14", "14-Mission"),
                    new Route("14R", "14R-Mission Rapid"),
                    new Route("14X", "14X-Mission Express"),
                    new Route("18", "18-46th Avenue"),
                    new Route("19", "19-Polk"),
                    new Route("21", "21-Hayes"),
                    new Route("22", "22-Fillmore"),
                    new Route("23", "23-Monterey"),
                    new Route("24", "24-Divisadero"),
                    new Route("25", "25-Treasure Island"),
                    new Route("27", "27-Bryant"),
                    new Route("28", "28-19th Avenue"),
                    new Route("28R", "28R-19th Avenue Rapid"),
                    new Route("29", "29-Sunset"),
                    new Route("30", "30-Stockton"),
                    new Route("30X", "30X-Marina Express"),
                    new Route("31", "31-Balboa"),
                    new Route("31AX", "31AX-Balboa A Express"),
                    new Route("31BX", "31BX-Balboa B Express"),
                    new Route("33", "33-Ashbury-18th"),
                    new Route("35", "35-Eureka"),
                    new Route("36", "36-Teresita"),
                    new Route("37", "37-Corbett"),
                    new Route("38", "38-Geary"),
                    new Route("38R", "38R-Geary Rapid"),
                    new Route("38AX", "38AX-Geary A Express"),
                    new Route("38BX", "38BX-Geary B Express"),
                    new Route("39", "39-Coit Tower"),
                    new Route("41", "41-Union"),
                    new Route("43", "43-Masonic"),
                    new Route("44", "44-O'Shaughnessy"),
                    new Route("45", "45-Union-Stockton"),
                    new Route("47", "47-Van Ness"),
                    new Route("48", "48-24th Street"),
                    new Route("49", "49-Van Ness-Mission"),
                    new Route("52", "52-Excelsior"),
                    new Route("54", "54-Felton"),
                    new Route("55", "55-16th Street"),
                    new Route("56", "56-Rutland"),
                    new Route("57", "57-Parkmerced"),
                    new Route("66", "66-Quintara"),
                    new Route("67", "67-Bernal Heights"),
                    new Route("76X", "76X-Marin Headlands Express"),
                    new Route("81X", "81X-Caltrain Express"),
                    new Route("82X", "82X-Levi Plaza Express"),
                    new Route("83X", "83X-Mid-Market Express"),
                    new Route("88", "88-Bart Shuttle"),
                    new Route("90", "90-San Bruno Owl"),
                    new Route("91", "91-Owl"),
                    new Route("K_OWL", "K-Owl"),
                    new Route("L_OWL", "L-Owl"),
                    new Route("M_OWL", "M-Owl"),
                    new Route("N_OWL", "N-Owl"),
                    new Route("T_OWL", "T-Owl"),
                    new Route("59", "Powell/Mason Cable Car"),
                    new Route("60", "Powell/Hyde Cable Car"),
                    new Route("61", "California Cable Car"),
                };
                break;
            default:
                return null;
        }

        return Arrays.asList(routes);
    }

    @Override
    public Route getRouteDetails(String agency, String routeTag) throws IOException {
        if (agency.equals("sf-muni") && routeTag.equals("F")) {
            final Route route = new Route("F", "F-Market & Wharves");
            List<Stop> stops = new LinkedList<>();
            stops.add(new Stop("5184", "Jones St & Beach St", 37.8072499, -122.41737));
            stops.add(new Stop("3092", "Beach St & Mason St", 37.80741, -122.4141199));
            stops.add(new Stop("3095", "Beach St & Stockton St", 37.8078399, -122.41081));
            stops.add(new Stop("4502", "The Embarcadero & Bay St", 37.8066299, -122.4060299));
            stops.add(new Stop("4529", "The Embarcadero & Sansome St", 37.8050199, -122.4033099));
            stops.add(new Stop("4516", "The Embarcadero & Greenwich St", 37.80296, -122.40103));
            stops.add(new Stop("4518", "The Embarcadero & Green St", 37.80061, -122.39892));
            stops.add(new Stop("4504", "The Embarcadero & Broadway", 37.7988999, -122.3974299));
            stops.add(new Stop("4534", "The Embarcadero & Washington St", 37.7963599, -122.3951799));
            stops.add(new Stop("7283", "The Embarcadero & Ferry Building", 37.7948299, -122.3937699));
            stops.add(new Stop("4726", "Ferry Plaza", 37.7940499, -122.39345));
            stops.add(new Stop("5669", "Market St & Drumm St", 37.7934699, -122.39618));
            stops.add(new Stop("5657", "Market St & Battery St", 37.7911099, -122.39907));
            stops.add(new Stop("5639", "Market St & 2nd St", 37.7893499, -122.40131));
            stops.add(new Stop("5678", "Market St & Kearny St", 37.78773, -122.4033699));
            stops.add(new Stop("5694", "Market St & Stockton St", 37.7858599, -122.40574));
            stops.add(new Stop("5655", "Market St & 5th St North", 37.7840799, -122.40799));
            stops.add(new Stop("5695", "Market St & Taylor St", 37.78232, -122.4102299));
            stops.add(new Stop("5656", "Market St & 7th St North", 37.7805699, -122.41244));
            stops.add(new Stop("5676", "Market St & Hyde St", 37.7791099, -122.41438));
            stops.add(new Stop("5680", "Market St & Larkin St", 37.7775999, -122.41624));
            stops.add(new Stop("5696", "Market St & Van Ness Ave", 37.7752399, -122.41918));
            stops.add(new Stop("5672", "Market St & Gough St", 37.77327, -122.4217699));
            stops.add(new Stop("5681", "Market St & Laguna St", 37.77095, -122.4246699));
            stops.add(new Stop("5659", "Market St & Buchanan St", 37.76979, -122.4261499));
            stops.add(new Stop("5661", "Market St & Church St", 37.7678299, -122.42863));
            stops.add(new Stop("5690", "Market St & Sanchez St", 37.7661899, -122.43071));
            stops.add(new Stop("5686", "Market St & Noe St", 37.7644899, -122.43281));
            stops.add(new Stop("33311", "17th St & Castro St", 37.7625199, -122.43487));
            stops.add(new Stop("3311", "17th St & Castro St", 37.7625199, -122.43487));
            stops.add(new Stop("5687", "Market St & Noe St", 37.7639599, -122.43332));
            stops.add(new Stop("5691", "Market St & Sanchez St", 37.7656899, -122.43114));
            stops.add(new Stop("5662", "Market St & Church St", 37.7672599, -122.42915));
            stops.add(new Stop("5668", "Market St & Dolores St", 37.76888, -122.4270999));
            stops.add(new Stop("5675", "Market St & Guerrero St", 37.7705699, -122.42497));
            stops.add(new Stop("5673", "Market St & Gough St", 37.7728799, -122.42199));
            stops.add(new Stop("5692", "Market St & South Van Ness Ave", 37.7750599, -122.41932));
            stops.add(new Stop("5652", "Market St & 9th St", 37.7774099, -122.41634));
            stops.add(new Stop("5651", "Market St & 8th St", 37.7786099, -122.41483));
            stops.add(new Stop("5650", "Market St & 7th St", 37.7803599, -122.41261));
            stops.add(new Stop("5647", "Market St & 6th St", 37.7820999, -122.4104));
            stops.add(new Stop("5645", "Market St & 5th St", 37.7838899, -122.40814));
            stops.add(new Stop("5643", "Market St & 4th St", 37.7856499, -122.40589));
            stops.add(new Stop("5640", "Market St & 3rd St", 37.7875299, -122.40352));
            stops.add(new Stop("5685", "Market St & New Montgomery St", 37.7886099, -122.40216));
            stops.add(new Stop("7264", "Market St & 1st St", 37.7909399, -122.39919));
            stops.add(new Stop("5682", "Market St & Main St", 37.7929799, -122.39663));
            stops.add(new Stop("4727", "Ferry Plaza", 37.7938999, -122.39345));
            stops.add(new Stop("4513", "The Embarcadero & Ferry Term", 37.79511, -122.39386));
            stops.add(new Stop("4532", "The Embarcadero & Washington St", 37.7970899, -122.3956699));
            stops.add(new Stop("4503", "The Embarcadero & Broadway", 37.7995499, -122.3978699));
            stops.add(new Stop("4517", "The Embarcadero & Green St", 37.8012599, -122.3993699));
            stops.add(new Stop("4515", "The Embarcadero & Greenwich St", 37.8032599, -122.4011099));
            stops.add(new Stop("7281", "Embarcadero & Sansome St", 37.80515, -122.40323));
            stops.add(new Stop("4501", "The Embarcadero & Bay St", 37.80695, -122.40628));
            stops.add(new Stop("4530", "Pier 39", 37.8083499, -122.41029));
            stops.add(new Stop("5174", "Jefferson St & Powell St", 37.8085899, -122.41336));
            stops.add(new Stop("5175", "Jefferson St & Taylor St", 37.8083199, -122.41551));
            stops.add(new Stop("35184", "Jones St & Beach St", 37.8072499, -122.41737));
            for (Stop stop : stops) {
                stop.setAgencyTag("sf-muni");
            }
            route.setStops(stops);
            return route;
        }

        return null;
    }

    @Override
    public List<Route> getDetailedRoutesForAgency(String agency) throws IOException {
        return Collections.singletonList(getRouteDetails("sf-muni", "F"));
    }

    @Override
    public List<Predictions> getPredictions(String agency, String stop) {
        if (agency.equals("sf-muni") && stop.equals("4727")) {
            final Predictions p = new Predictions();
            p.setAgencyTitle("San Fransisco Muni");
            p.setRouteTag("F");
            p.setRouteTitle("F-Market & Wharves");
            p.setStopTag("4727");
            p.setStopTitle("Ferry Plaza");

            final PredictionDirection direction = new PredictionDirection();
            
            
            p.setDirections(Collections.singletonList(direction));
            return Collections.singletonList(p);
        }

        return null;
    }
}
