package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.model.Predictions;
import java.util.List;
import javax.ws.rs.core.GenericType;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class PredictionServiceTest extends ApiTest {
    
    @Test
    public void testGetPredictions() {
        System.out.println("GET predictions/sf-muni/4727");

        List<Predictions> predictions = target.path("predictions/sf-muni/4727").request().get(new GenericType<List<Predictions>>() {});
        assertEquals(1, predictions.size());
    }
}
