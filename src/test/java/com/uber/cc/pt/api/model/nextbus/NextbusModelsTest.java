package com.uber.cc.pt.api.model.nextbus;

import com.uber.cc.pt.api.client.NextBusClient;
import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.model.Route;
import com.uber.cc.pt.api.model.Stop;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class NextbusModelsTest {

    private static String agencyListText;
    private static String muniRouteListText;
    private static String muniRouteDetailsFLineText;
    private static String muniPredictionF4727Text;
    
    public NextbusModelsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        InputStream is = NextbusModelsTest.class.getResourceAsStream("/models/nextbus_agencies.xml");
        int b;
        while ((b = is.read()) != -1) {
            baos.write(b);
        }
        is.close();
        baos.close();
        agencyListText = baos.toString("UTF-8");

        baos = new ByteArrayOutputStream(1024);
        is = NextbusModelsTest.class.getResourceAsStream("/models/nextbus_muni_routes.xml");
        while ((b = is.read()) != -1) {
            baos.write(b);
        }
        is.close();
        baos.close();
        muniRouteListText = baos.toString("UTF-8");

        baos = new ByteArrayOutputStream(1024);
        is = NextbusModelsTest.class.getResourceAsStream("/models/nextbus_route_detail_F.xml");
        while ((b = is.read()) != -1) {
            baos.write(b);
        }
        is.close();
        baos.close();
        muniRouteDetailsFLineText = baos.toString("UTF-8");

        baos = new ByteArrayOutputStream(1024);
        is = NextbusModelsTest.class.getResourceAsStream("/models/nextbus_muni_prediction_F_4727.xml");
        while ((b = is.read()) != -1) {
            baos.write(b);
        }
        is.close();
        baos.close();
        muniPredictionF4727Text = baos.toString("UTF-8");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testParseAgencyList() throws Exception {
        System.out.println("Parse agency list XML");
        
        BodyResponse instance = (BodyResponse) new NextBusClient().getUnmarshaller().unmarshal(new StringReader(agencyListText));
        assertNotNull(instance);
        assertNotNull(instance.getAgencies());

        final Agency firstAgency = instance.getAgencies().get(0);
        assertEquals("actransit", firstAgency.getTag());
        assertEquals("AC Transit", firstAgency.getTitle());

        final Agency secondAgency = instance.getAgencies().get(1);
        assertEquals("jhu-apl", secondAgency.getTag());
        assertEquals("APL", secondAgency.getTitle());
    }

    @Test
    public void testParseMuniRouteList() throws Exception {
        System.out.println("Parse sf-muni route list XML");

        BodyResponse instance = (BodyResponse) new NextBusClient().getUnmarshaller().unmarshal(new StringReader(muniRouteListText));
        assertNotNull(instance);
        assertNotNull(instance.getRoutes());

        final Route firstRoute = instance.getRoutes().get(0);
        assertEquals("F", firstRoute.getTag());
        assertEquals("F-Market & Wharves", firstRoute.getTitle());
    }

    @Test
    public void testParseMuniRouteDetailsFLine() throws Exception {
        System.out.println("Parse sf-muni route details F-Line XML");

        BodyResponse instance = (BodyResponse) new NextBusClient().getUnmarshaller().unmarshal(new StringReader(muniRouteDetailsFLineText));
        assertNotNull(instance);
        assertNotNull(instance.getRoutes());

        final Route route = instance.getRoutes().get(0);
        assertEquals("F", route.getTag());
        assertEquals("F-Market & Wharves", route.getTitle());
        assertNotNull(route.getStops());
        assertEquals(59, route.getStops().size());
        final Stop firstStop = route.getStops().get(0);
        assertEquals("5184", firstStop.getTag());
        assertEquals("Jones St & Beach St", firstStop.getTitle());
        assertEquals(37.8072499, firstStop.getLat(), .01);
        assertEquals(-122.41737, firstStop.getLon(), .01);
        assertEquals("15184", firstStop.getId());
    }

    @Test
    public void testParseMuniRoutePredictionFLineStop4727() throws Exception {
        System.out.println("Parse sf-muni route prediction for F-Line Stop 4727 XML");

        BodyResponse instance = (BodyResponse) new NextBusClient().getUnmarshaller().unmarshal(new StringReader(muniPredictionF4727Text));
        assertNotNull(instance);
        assertNotNull(instance.getPredictions());
        assertEquals("San Francisco Muni", instance.getPredictions().get(0).getAgencyTitle());
        assertEquals("F-Market & Wharves", instance.getPredictions().get(0).getRouteTitle());
        assertNotNull(instance.getPredictions().get(0).getDirections());
        assertEquals(1, instance.getPredictions().get(0).getDirections().size());
    }
}
