package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.Main;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.junit.After;
import org.junit.Before;

/**
 * Base class for API tests. Spins up an API server running locally, and
 * tears it down after the tests complete.
 */
public class ApiTest {

    public static final String BASE_URI = "http://localhost:8181/pt-test/";

    protected HttpServer server;
    protected WebTarget target;

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer(BASE_URI);
        // create the client
        Client c = ClientBuilder.newClient();
        c.register(new MoxyJsonFeature());
        target = c.target(BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }
}
