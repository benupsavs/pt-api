package com.uber.cc.pt.api.model.org511;

import com.uber.cc.pt.api.client.Org511Client;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Org511ModelsTest {

    private static String agencyListText;
    
    public Org511ModelsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        InputStream is = Org511ModelsTest.class.getResourceAsStream("/models/511_agencies.xml");
        int b;
        while ((b = is.read()) != -1) {
            baos.write(b);
        }
        is.close();
        baos.close();
        agencyListText = baos.toString("UTF-8");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testParseAgencyList() throws Exception {
        System.out.println("parse agency list XML");
        
        RTTResponse instance = (RTTResponse) new Org511Client().getUnmarshaller().unmarshal(new StringReader(agencyListText));
        assertNotNull(instance.getAgencyList());
        assertNotNull(instance.getAgencyList().getAgencies());

        final Agency firstAgency = instance.getAgencyList().getAgencies().get(0);
        assertEquals("AC Transit", firstAgency.getName());
        assertTrue(firstAgency.isWithDirection());
        assertEquals("Bus", firstAgency.getMode());

        final Agency secondAgency = instance.getAgencyList().getAgencies().get(1);
        assertEquals("BART", secondAgency.getName());
        assertFalse(secondAgency.isWithDirection());
        assertEquals("Rail", secondAgency.getMode());
    }
}
