package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.model.Agency;
import java.util.List;
import javax.ws.rs.core.GenericType;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AgencyServiceTest extends ApiTest {

    @Test
    public void testGetAgencies() {
        System.out.println("GET agency");
        List<Agency> agencies = target.path("agency").request().get(new GenericType<List<Agency>>() {});
        assertEquals(63, agencies.size());
    }
}
