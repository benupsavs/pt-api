package com.uber.cc.pt.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a message intended for the end user.
 */
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {

    private String text;

    /**
     * Gets the message text.
     * @return the message text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the message text.
     * @param text the message text
     */
    public void setText(String text) {
        this.text = text;
    }
}
