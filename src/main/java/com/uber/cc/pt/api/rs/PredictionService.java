/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.client.BaseTransitClient;
import com.uber.cc.pt.api.client.TransitClient;
import com.uber.cc.pt.api.model.Predictions;
import com.uber.cc.pt.api.persistence.Persistence;
import com.uber.cc.pt.api.persistence.PersistenceEngine;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;

/**
 * REST service related to transport agency requests.
 */
@Path("predictions")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PredictionService {
    
    private static final Logger LOG = Logger.getLogger(PredictionService.class.getName());

    private final ExecutorService predictionsPool = Executors.newCachedThreadPool();
    private final PersistenceEngine persistence;
    private final TransitClient transportClient;

    public PredictionService() {
        persistence = Persistence.getPersistenceEngine();
        transportClient = BaseTransitClient.getTransitClient();
    }

    @GET
    @Path("{agency}/{stop}")
    public void getPredictions(final @PathParam("agency") String agency,
            final @PathParam("stop") String stop,
            final @Suspended AsyncResponse asyncResponse) {
        predictionsPool.submit(new Runnable() {

            @Override
            public void run() {
                try {
                    final List<Predictions> predictions = transportClient.getPredictions(agency, stop);
                    asyncResponse.resume(new GenericEntity<List<Predictions>>(predictions == null ? Collections.<Predictions>emptyList() : predictions) {});
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, "Unable to fetch predictions", ex);
                    asyncResponse.resume(ex);
                }
            }
        });
    }
}


