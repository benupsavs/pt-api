package com.uber.cc.pt.api.model.nextbus;

import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.model.Predictions;
import com.uber.cc.pt.api.model.Route;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing the NextBus base response.
 * Each element is from a different response, so only one will normally be
 * non-null.
 */
@XmlRootElement(name = "body")
@XmlAccessorType(XmlAccessType.FIELD)
public class BodyResponse {

    @XmlElement(name = "agency")
    private List<Agency> agencies;

    @XmlElement(name = "route")
    private List<Route> routes;

    @XmlElement(name = "predictions")
    private List<Predictions> predictions;

    /**
     * Gets the agencies returned in agency requests.
     * @return a list of agencies
     */
    public List<Agency> getAgencies() {
        return agencies;
    }

    /**
     * Sets the agencies returned in agency requests.
     * @param agencies a list of agencies
     */
    public void setAgencies(List<Agency> agencies) {
        this.agencies = agencies;
    }

    /**
     * Gets the routes returned in route requests.
     * @return a list of routes
     */
    public List<Route> getRoutes() {
        return routes;
    }

    /**
     * Sets routes returned in route requests.
     * @param routes a list of routes
     */
    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    /**
     * Gets predictions returned in prediction requests.
     * @return a list of predictions
     */
    public List<Predictions> getPredictions() {
        return predictions;
    }

    /**
     * Sets predictions returned in prediction requests
     * @param predictions a list of predictions
     */
    public void setPredictions(List<Predictions> predictions) {
        this.predictions = predictions;
    }
}
