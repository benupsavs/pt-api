package com.uber.cc.pt.api.persistence;

import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.model.Route;
import com.uber.cc.pt.api.model.Stop;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Persistence engine implementation that stores data in memory. This
 * implementation is simple and has no expiration policy for the data.
 */
public class MemoryPersistenceEngine implements PersistenceEngine {

    private List<Agency> agencies;
    private final Map<String, List<Route>> agencyRoutes = new HashMap<>(128);
    private final Map<String, Route> routesByTag = new HashMap<>(128);
    private final Map<String, Stop> stopsByTag = new HashMap<>(1024);

    @Override
    public List<Agency> getAgencies() {
        return agencies;
    }

    @Override
    public void setAgencies(List<Agency> agencies) {
        if (agencies != null) {
            this.agencies = new ArrayList<>(agencies);
        }
    }

    @Override
    public List<Route> getRoutesForAgency(String agencyTag) {
        return agencyRoutes.get(agencyTag);
    }

    @Override
    public void setRoutesForAgency(String agencyTag, List<Route> routes) {
        this.agencyRoutes.put(agencyTag, routes);
        if (routes != null) {
            synchronized (routesByTag) {
                for (Route route : routes) {
                    if (!routesByTag.containsKey(agencyTag)) {
                        routesByTag.put(route.getTag(), route);
                    }
                }
            }
        }
    }

    @Override
    public void addStop(Stop stop) {
        stopsByTag.put(stop.getTag(), stop);
    }

    @Override
    public int countAllStops() {
        return stopsByTag.size();
    }

    @Override
    public List<Stop> getClosestStops(final double lat, final double lon, int numberOfStops) {
        if (numberOfStops < 1) {
            numberOfStops = 1;
        }
        final List<Stop> sortedStops = new ArrayList<>(Math.max(stopsByTag.size(), 128));
        sortedStops.addAll(stopsByTag.values());
        Collections.sort(sortedStops, new Comparator<Stop>() {

            @Override
            public int compare(Stop o1, Stop o2) {
                return Double.valueOf(distance(lat, lon, o1.getLat(), o1.getLon()))
                        .compareTo(distance(lat, lon, o2.getLat(), o2.getLon()));
            }
        });

        return sortedStops.subList(0, Math.min(numberOfStops, sortedStops.size()));
    }

    /**
     * Gets the <em>Haversine</em> distance formula between two coordinates.
     * @param lat1 the first position's latitude
     * @param lon1 the first position's longitude
     * @param lat2 the second position's latitude
     * @param lon2 the second position's longitude
     * @return the distance over the earth's surface
     */
    public double distance(double lat1, double lon1, double lat2, double lon2) {
        final int r = 6371000;
        double lat1Radians = Math.toRadians(lat1);
        double lat2Radians = Math.toRadians(lat2);
        double latDeltaRadians = Math.toRadians(lat2 - lat1);
        double lonDeltaRadians = Math.toRadians(lon2 - lon1);

        double a = Math.sin(latDeltaRadians / 2) * Math.sin(latDeltaRadians / 2)
                + Math.cos(lat1Radians) * Math.cos(lat2Radians)
                * Math.sin(lonDeltaRadians / 2) * Math.sin(lonDeltaRadians / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return r * c;
    }
}
