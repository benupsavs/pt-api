package com.uber.cc.pt.api.client;

import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.model.Predictions;
import com.uber.cc.pt.api.model.Route;
import java.io.IOException;
import java.util.List;

/**
 * Common interface for clients that call transport services.
 */
public interface TransitClient {

    /**
     * Gets the agencies that this transport client supports.
     * @return a list of agencies
     * @throws IOException if an I/O error occurs
     */
    List<Agency> getAgencies() throws IOException;

    /**
     * Gets a list of routes for the given agency.
     * @param agency the tag of the agency to get routes for
     * @return a list of routes
     * @throws IOException if an I/O error occurs
     */
    List<Route> getRoutesForAgency(String agency) throws IOException;

    /**
     * Gets details for the route with the given agency and tag. These details
     * include the locations of the stops along the route.
     * @param agency the tag of the agency that the route belongs to
     * @param routeTag the tag of the route
     * @return a route with details attached, or {@code null} if the route
     * could not be loaded
     * @throws IOException if an I/O error occurs
     */
    Route getRouteDetails(String agency, String routeTag) throws IOException;

    /**
     * Gets details for all routes in the agency with the given tag.
     * @param agency the agency tag
     * @return a list of routes
     * @throws IOException if an I/O error occurs
     */
    List<Route> getDetailedRoutesForAgency(String agency) throws IOException;

    /**
     * Gets a list of predictions for the given stop in the given agency.
     * @param agency the agency tag
     * @param stop the stop tag
     * @return a list of predictions - one for each route
     * @throws IOException if an I/O error occurs
     */
    List<Predictions> getPredictions(String agency, String stop) throws IOException;
}
