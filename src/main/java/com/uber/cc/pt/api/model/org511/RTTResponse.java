package com.uber.cc.pt.api.model.org511;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing the 511 base response.
 */
@XmlRootElement(name="RTT")
@XmlAccessorType(XmlAccessType.FIELD)
public class RTTResponse implements Serializable {

    @XmlElement(name = "AgencyList")
    private AgencyList agencyList;

    public AgencyList getAgencyList() {
        return agencyList;
    }

    public void setAgencyList(AgencyList agencyList) {
        this.agencyList = agencyList;
    }
}