package com.uber.cc.pt.api.persistence;

import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.model.Route;
import com.uber.cc.pt.api.model.Stop;
import java.util.List;

/**
 * Interface for persistence engine implementations.
 */
public interface PersistenceEngine {

    /**
     * Gets all agencies in the database.
     * @return a list of agencies, or {@null} if no agencies are in the
     * database
     */
    List<Agency> getAgencies();

    /**
     * Stores the given list of agencies in the database.
     * @param agencies a list of agencies
     */
    void setAgencies(List<Agency> agencies);

    /**
     * Gets the list of routes for the given agency tag.
     * @param agencyTag the tag for the agency to get routes for
     * @return a list of routes, or {@code null} if no routes exist with
     * the given tag
     */
    List<Route> getRoutesForAgency(String agencyTag);

    /**
     * Sets the given list of routes for the agency with the given tag.
     * @param agencyTag the tag of the agency to set routes for
     * @param routes a list of routes
     */
    void setRoutesForAgency(String agencyTag, List<Route> routes);

    /**
     * Adds the given stop to the data store.
     * @param stop the stop to add
     */
    void addStop(Stop stop);
    
    /**
     * Returns the number of stops in the database.
     * @return the total number of stops
     */
    int countAllStops();

    /**
     * Gets the closest stops in the database.
     * @param lat the latitude to find stops near
     * @param lon the longitude to find stops near
     * @param numberOfStops the number of stops to return
     * @return a list of stops
     */
    List<Stop> getClosestStops(double lat, double lon, int numberOfStops);
}
