package com.uber.cc.pt.api.client;

import com.uber.cc.pt.api.model.*;
import com.uber.cc.pt.api.model.nextbus.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Base class for JAXB HTTP transport client implementations.
 * Implementations should override all supported operations.
 */
public abstract class BaseTransitClient implements TransitClient {

    private static final Logger LOG = Logger.getLogger(BaseTransitClient.class.getName());

    public Unmarshaller getUnmarshaller() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(new Class[] {Agency.class, Route.class, Stop.class, BodyResponse.class});
        return context.createUnmarshaller();
    }

    URL getURL(String baseURL, String path, String... extraParameters) {
        StringBuilder urlBuilder = new StringBuilder(128).append(baseURL).append(path);
        boolean containsParam = baseURL.contains("?");
        for (int paramCount = 0; paramCount < extraParameters.length; paramCount += 2) {
            if (!containsParam && paramCount == 0) {
                urlBuilder.append('?');
            } else {
                urlBuilder.append('&');
            }

            urlBuilder.append(extraParameters[paramCount]).append('=').append(extraParameters[paramCount + 1]);
        }
        try {
            return new URL(urlBuilder.toString());
        } catch (MalformedURLException ex) {
            LOG.log(Level.SEVERE, "Malformed URL: {0}", urlBuilder);
            throw new RuntimeException("Internal error");
        }
    }

    public <T> T makeRequest(String baseURL, String path, Class<T> modelClass, String... extraParameters) throws IOException {
        final URL url = getURL(baseURL, path, extraParameters);
        URLConnection c = url.openConnection();
        InputStream is = c.getInputStream();
        try {
            final Unmarshaller unmarshaller = getUnmarshaller();
            return modelClass.cast(unmarshaller.unmarshal(is));
        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, "Transit service data error", ex);
            return null;
        } finally {
            try {
                is.close();
            } catch (Exception ex) {}
        }
    }

    /**
     * Gets the transit client to use for requests for public transportation
     * data.
     * @return an instance of a transport client
     */
    public static TransitClient getTransitClient() {
        TransitClient foundClient = null;
        for (TransitClient c : ServiceLoader.load(TransitClient.class)) {
            foundClient = c;
            break;
        }

        if (foundClient == null) {
            foundClient = NextBusClient.getInstance();
        }

        return foundClient;
    }

    // Default implementations
    @Override
    public List<Agency> getAgencies() throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public List<Route> getRoutesForAgency(String agency) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Route getRouteDetails(String agency, String routeTag) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public List<Route> getDetailedRoutesForAgency(String agency) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public List<Predictions> getPredictions(String agency, String stop) throws IOException {
        throw new UnsupportedOperationException("Not supported.");
    }
}
