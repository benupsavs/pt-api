package com.uber.cc.pt.api.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model for a transit route.
 */
@XmlRootElement(name = "route")
@XmlAccessorType(XmlAccessType.FIELD)
public class Route {

    @XmlAttribute
    private String tag;

    @XmlAttribute
    private String title;

    @XmlAttribute
    private String shortTitle;

    @XmlElement(name = "stop")
    private List<Stop> stops;

    public Route() {
    }
    
    public Route(String tag, String title) {
        this(tag, title, title);
    }

    public Route(String tag, String title, String shortTitle) {
        this.tag = tag;
        this.title = title;
        this.shortTitle = shortTitle;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops == null ? null : new ArrayList<>(stops);
    }
}
