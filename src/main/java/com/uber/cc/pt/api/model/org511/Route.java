package com.uber.cc.pt.api.model.org511;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a 511 transport route.
 */
@XmlRootElement(name = "Route")
@XmlAccessorType(XmlAccessType.FIELD)
public class Route {

    public Route() {
    }

    public Route(String name, String code) {
        this.name = name;
        this.code = code;
    }

    @XmlAttribute(name = "Name")
    private String name;

    @XmlAttribute(name = "Code")
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
