package com.uber.cc.pt.api.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a collection of predictions for a direction of
 * travel.
 */
@XmlRootElement(name = "direction")
@XmlAccessorType(XmlAccessType.FIELD)
public class PredictionDirection {

    @XmlAttribute
    private String title;

    @XmlElement(name = "prediction")
    private List<Prediction> predictions;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }
}
