package com.uber.cc.pt.api.model.org511;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a 511 route direction list.
 */
@XmlRootElement(name = "RouteDirectionList")
@XmlAccessorType(XmlAccessType.FIELD)
public class RouteDirectionList {

    @XmlAttribute(name = "RouteDirectionList")
    private List<RouteDirection> routeDirections;

    public List<RouteDirection> getRouteDirections() {
        return routeDirections;
    }

    public void setRouteDirections(List<RouteDirection> routeDirections) {
        this.routeDirections = routeDirections;
    }
}
