package com.uber.cc.pt.api.client;

import com.uber.cc.pt.api.model.org511.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Web service client for 511.org public transportation departure times.
 * This implementation is incomplete since 511.org doesn't provide locations
 * for their stops. A couple of the methods are implemented to illustrate
 * how different service clients may be written.
 */
public class Org511Client extends BaseTransitClient {

    private static volatile Org511Client instance;
    private static final Logger LOG = Logger.getLogger(Org511Client.class.getName());

    private final String securityToken;
    private final String baseURL;

    public Org511Client() {
        Properties config = new Properties();
        InputStream is = new BufferedInputStream(Org511Client.class.getResourceAsStream("/config.properties"));
        try {
            config.load(is);
            securityToken = URLEncoder.encode(config.getProperty("org.511.key"), "UTF-8");
            baseURL = config.getProperty("org.511.baseurl");
        } catch (IOException ex) {
            throw new RuntimeException("Unable to load config.properties", ex);
        } finally {
            try {
                is.close();
            } catch (Exception ex) {}
        }
    }

    /**
     * Gets a singleton instance of the 511.org client.
     * @return an instance of {@code Org511Client}.
     */
    public static Org511Client getInstance() {
        // Efficient double checked locking implementation,
        // which only accesses the volatile field once under normal conditions
        Org511Client c = instance;
        if (c == null) {
            synchronized (Org511Client.class) {
                if (instance == null) {
                    instance = new Org511Client();
                }

                c = instance;
            }
        }

        return c;
    }

    /**
     * {@inheritDoc}
     * <p>
     * This implementation includes the 511.org security token
     * with all requests, as a query parameter.
     */
    @Override
    URL getURL(String baseURL, String path, String... extraParameters) {
        String[] paramsWithToken = new String[extraParameters == null ? 0 : extraParameters.length + 2];
        paramsWithToken[0] = "token";
        paramsWithToken[1] = securityToken;
        if (extraParameters != null) {
            System.arraycopy(extraParameters, 0, paramsWithToken, 2, extraParameters.length);
        }
        return super.getURL(baseURL, path, paramsWithToken);
    }

    @Override
    public List<com.uber.cc.pt.api.model.Agency> getAgencies() throws IOException {
        final RTTResponse response = makeRequest(baseURL, "/GetAgencies.aspx", RTTResponse.class, (String[]) null);
        if (response == null) {
            return null;
        } else {
            final List<com.uber.cc.pt.api.model.Agency> converted = new LinkedList<>();
            for (Agency a : response.getAgencyList().getAgencies()) {
                com.uber.cc.pt.api.model.Agency ca = new com.uber.cc.pt.api.model.Agency();
                ca.setTag(a.getName());
                ca.setTitle(a.getName());
                converted.add(ca);
            }
            return converted;
        }
    }

    @Override
    public List<com.uber.cc.pt.api.model.Route> getRoutesForAgency(String agency) throws IOException {
        final RTTResponse response = makeRequest(baseURL, "/GetRoutesForAgency.aspx", RTTResponse.class, "agencyName", agency);
        final List<com.uber.cc.pt.api.model.Route> converted = new LinkedList<>();
        for (Route r : response.getAgencyList().getAgencies().get(0).getRouteList().getRouteList()) {
            com.uber.cc.pt.api.model.Route cr = new com.uber.cc.pt.api.model.Route();
            cr.setTag(r.getCode());
            cr.setTitle(r.getName());
            cr.setShortTitle(r.getName());
            converted.add(cr);
        }
        return converted;
    }

    @Override
    public Unmarshaller getUnmarshaller() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(new Class[] {Agency.class, AgencyList.class, RTTResponse.class});
        return context.createUnmarshaller();
    }
}
