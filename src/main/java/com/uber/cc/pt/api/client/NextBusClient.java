package com.uber.cc.pt.api.client;

import com.uber.cc.pt.api.model.*;
import com.uber.cc.pt.api.model.nextbus.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * NextBus transport client implementation.
 */
public class NextBusClient extends BaseTransitClient {

    private static volatile NextBusClient instance;
    private static final Logger LOG = Logger.getLogger(Org511Client.class.getName());

    private final String baseURL;

    public NextBusClient() {
        Properties config = new Properties();
        InputStream is = new BufferedInputStream(Org511Client.class.getResourceAsStream("/config.properties"));
        try {
            config.load(is);
            baseURL = config.getProperty("com.nextbus.baseurl");
        } catch (IOException ex) {
            throw new RuntimeException("Unable to load config.properties", ex);
        } finally {
            try {
                is.close();
            } catch (Exception ex) {}
        }
    }

    /**
     * Gets a singleton instance of the 511.org client.
     * @return an instance of {@code Org511Client}.
     */
    public static NextBusClient getInstance() {
        // Efficient double checked locking implementation,
        // which only accesses the volatile field once under normal conditions
        NextBusClient c = instance;
        if (c == null) {
            synchronized (Org511Client.class) {
                if (instance == null) {
                    instance = new NextBusClient();
                }

                c = instance;
            }
        }

        return c;
    }

    @Override
    public List<Agency> getAgencies() throws IOException {
        return makeRequest(baseURL, "agencyList", BodyResponse.class).getAgencies();
    }

    @Override
    public List<Route> getRoutesForAgency(String agency) throws IOException {
        return makeRequest(baseURL, "routeList", BodyResponse.class, "a", agency).getRoutes();
    }

    @Override
    public Route getRouteDetails(String agency, String routeTag) throws IOException {
        final BodyResponse response = makeRequest(baseURL, "routeConfig", BodyResponse.class, "a", agency, "r", routeTag);
        if (response == null || response.getRoutes().isEmpty()) {
            return null;
        } else {
            return response.getRoutes().get(0);
        }
    }

    @Override
    public List<Route> getDetailedRoutesForAgency(String agency) throws IOException {
        final BodyResponse response = makeRequest(baseURL, "routeConfig", BodyResponse.class, "a", agency);
        if (response == null || response.getRoutes() == null || response.getRoutes().isEmpty()) {
            return Collections.emptyList();
        } else {
            return response.getRoutes();
        }
    }

    @Override
    public List<Predictions> getPredictions(String agency, String stop) throws IOException {
        final BodyResponse response = makeRequest(baseURL, "predictions", BodyResponse.class, "a", agency, "stopId", stop);
        if (response == null || response.getPredictions() == null || response.getPredictions().isEmpty()) {
            return Collections.emptyList();
        } else {
            return response.getPredictions();
        }
    }
}
