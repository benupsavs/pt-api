package com.uber.cc.pt.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing an Agency.
 */
@XmlRootElement(name = "agency")
@XmlAccessorType(XmlAccessType.FIELD)
public class Agency {

    @XmlAttribute
    private String tag;

    @XmlAttribute
    private String title;

    @XmlAttribute
    private String shortTitle;

    @XmlAttribute
    private String regionTitle;
    
    public Agency() {
    }

    public Agency(String tag, String title, String regionTitle) {
        this(tag, title, title, regionTitle);
    }

    public Agency(String tag, String title, String shortTitle, String regionTitle) {
        this.tag = tag;
        this.title = title;
        this.shortTitle = shortTitle;
        this.regionTitle = regionTitle;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public String getRegionTitle() {
        return regionTitle;
    }

    public void setRegionTitle(String regionTitle) {
        this.regionTitle = regionTitle;
    }
}
