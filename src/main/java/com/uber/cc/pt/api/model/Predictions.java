package com.uber.cc.pt.api.model;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a collection of transit time predictions.
 */
@XmlRootElement(name = "predictions")
@XmlAccessorType(XmlAccessType.FIELD)
public class Predictions {

    @XmlAttribute
    private String agencyTitle;

    @XmlAttribute
    private String routeTitle;

    @XmlAttribute
    private String routeTag;

    @XmlAttribute
    private String stopTitle;

    @XmlAttribute
    private String stopTag;

    @XmlElement(name = "direction")
    private List<PredictionDirection> directions;

    @XmlElement(name = "message")
    private List<Message> messages;

    public String getAgencyTitle() {
        return agencyTitle;
    }

    public void setAgencyTitle(String agencyTitle) {
        this.agencyTitle = agencyTitle;
    }

    public String getRouteTitle() {
        return routeTitle;
    }

    public void setRouteTitle(String routeTitle) {
        this.routeTitle = routeTitle;
    }

    public String getRouteTag() {
        return routeTag;
    }

    public void setRouteTag(String routeTag) {
        this.routeTag = routeTag;
    }

    public String getStopTitle() {
        return stopTitle;
    }

    public void setStopTitle(String stopTitle) {
        this.stopTitle = stopTitle;
    }

    public String getStopTag() {
        return stopTag;
    }

    public void setStopTag(String stopTag) {
        this.stopTag = stopTag;
    }

    public List<PredictionDirection> getDirections() {
        return directions;
    }

    public void setDirections(List<PredictionDirection> directions) {
        this.directions = directions;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
