package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.client.BaseTransitClient;
import com.uber.cc.pt.api.client.TransitClient;
import com.uber.cc.pt.api.model.Predictions;
import com.uber.cc.pt.api.model.Route;
import com.uber.cc.pt.api.model.Stop;
import com.uber.cc.pt.api.persistence.Persistence;
import com.uber.cc.pt.api.persistence.PersistenceEngine;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;

/**
 * REST service related to transport agency requests.
 */
@Path("route")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RouteService {

    private static final Logger LOG = Logger.getLogger(RouteService.class.getName());

    private final PersistenceEngine persistence;
    private final TransitClient transportClient;

    public RouteService() {
        persistence = Persistence.getPersistenceEngine();
        transportClient = BaseTransitClient.getTransitClient();
    }

    /**
     * GET request for routes for an agency.
     * @param agencyTag the tag for the agency to get routes for
     * @param asyncResponse the JAX-RS async response
     */
    @GET
    @Path("{agency}")
    public void getRoutesForAgency(@PathParam("agency") final String agencyTag,
            final @Suspended AsyncResponse asyncResponse) {
        final List<Route> cachedRoutes = persistence.getRoutesForAgency(agencyTag);
        if (cachedRoutes == null) {
            new Thread("Fetch agency routes") {

                @Override
                public void run() {
                    try {
                        final List<Route> routes = transportClient.getRoutesForAgency(agencyTag);
                        
                        persistence.setRoutesForAgency(agencyTag, routes);
                        asyncResponse.resume(new GenericEntity<List<Route>>(routes == null ? Collections.<Route>emptyList() : routes) {});
                    } catch (Exception ex) {
                        LOG.log(Level.SEVERE, "Unable to fetch routes", ex);
                        asyncResponse.resume(ex);
                    }
                }
            }.start();
        } else {
            asyncResponse.resume(new GenericEntity<List<Route>>(cachedRoutes) {});
        }
    }

    @GET
    @Path("closest/{lat},{lon}")
    public List<Stop> getClosestStops(@PathParam("lat") double lat, @PathParam("lon") double lon) {
        return persistence.getClosestStops(lat, lon, 5);
    }
}
