package com.uber.cc.pt.api.model;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * JAXB adapter to parse and render booleans as a string with any
 * capitalization.
 * <p>
 * Example: {@code &quot;True&quot;}.
 */
public class StringBooleanAdapter extends XmlAdapter<String, Boolean> {

    @Override
    public Boolean unmarshal(String v) throws Exception {
        if (v == null) {
            return null;
        }

        return v.equalsIgnoreCase("true");
    }

    @Override
    public String marshal(Boolean v) throws Exception {
        if (v == null) {
            return null;
        }

        return v.toString();
    }
    
}
