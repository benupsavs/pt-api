package com.uber.cc.pt.api.model;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a stop on a transit route.
 */
@XmlRootElement(name = "stop")
@XmlAccessorType(XmlAccessType.FIELD)
public class Stop {

    @XmlAttribute
    private String tag;

    @XmlAttribute
    private String title;

    @XmlAttribute
    private String shortTitle;

    @XmlAttribute
    private double lat;

    @XmlAttribute
    private double lon;

    @XmlAttribute(name = "agency")
    private String agencyTag;

    @XmlAttribute(name = "stopId")
    private String id;

    public Stop() {
    }

    public Stop(String tag, String title, double lat, double lon) {
        this(tag, title, title, lat, lon);
    }

    public Stop(String tag, String title, String shortTitle, double lat, double lon) {
        this(tag, title, shortTitle, lat, lon, null, tag);
    }

    public Stop(String tag, String title, String shortTitle, double lat, double lon, String agencyTag, String id) {
        this.tag = tag;
        this.title = title;
        this.shortTitle = shortTitle;
        this.lat = lat;
        this.lon = lon;
        this.agencyTag = agencyTag;
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.tag);
        hash = 47 * hash + Objects.hashCode(this.agencyTag);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stop other = (Stop) obj;
        if (!Objects.equals(this.tag, other.tag)) {
            return false;
        }
        if (!Objects.equals(this.agencyTag, other.agencyTag)) {
            return false;
        }
        return true;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getAgencyTag() {
        return agencyTag;
    }

    public void setAgencyTag(String agencyTag) {
        this.agencyTag = agencyTag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
