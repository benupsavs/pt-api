package com.uber.cc.pt.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a prediction of a transit vehicle's arrival at a
 * stop.
 */
@XmlRootElement(name = "prediction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Prediction {

    @XmlAttribute
    private long epochTime;

    @XmlAttribute
    private int seconds;

    @XmlAttribute
    private int minutes;

    @XmlAttribute(name = "isDeparture")
    private boolean departure;

    @XmlAttribute(name = "dirTag")
    private String directionTag;

    @XmlAttribute
    private String vehicle;

    @XmlAttribute
    private String block;

    @XmlAttribute
    private String tripTag;

    @XmlAttribute
    private boolean affectedByLayover;

    public long getEpochTime() {
        return epochTime;
    }

    public void setEpochTime(long epochTime) {
        this.epochTime = epochTime;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public boolean isDeparture() {
        return departure;
    }

    public void setDeparture(boolean departure) {
        this.departure = departure;
    }

    public String getDirectionTag() {
        return directionTag;
    }

    public void setDirectionTag(String directionTag) {
        this.directionTag = directionTag;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getTripTag() {
        return tripTag;
    }

    public void setTripTag(String tripTag) {
        this.tripTag = tripTag;
    }

    public boolean isAffectedByLayover() {
        return affectedByLayover;
    }

    public void setAffectedByLayover(boolean affectedByLayover) {
        this.affectedByLayover = affectedByLayover;
    }
}
