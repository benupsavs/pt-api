package com.uber.cc.pt.api.model.org511;

import com.uber.cc.pt.api.model.StringBooleanAdapter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * JAXB model representing a 511 agency.
 */
@XmlRootElement(name = "agency")
@XmlAccessorType(XmlAccessType.FIELD)
public class Agency {

    public Agency() {
    }

    public Agency(String name, boolean withDirection, String mode) {
        this.name = name;
        this.withDirection = withDirection;
        this.mode = mode;
    }

    @XmlAttribute(name = "Name")
    private String name;

    @XmlJavaTypeAdapter(StringBooleanAdapter.class)
    @XmlAttribute(name = "HasDirection")
    private Boolean withDirection;

    @XmlAttribute(name = "Mode")
    private String mode;

    @XmlElement(name = "RouteList")
    private RouteList routeList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isWithDirection() {
        return withDirection;
    }

    public void setWithDirection(Boolean withDirection) {
        this.withDirection = withDirection;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public RouteList getRouteList() {
        return routeList;
    }

    public void setRouteList(RouteList routeList) {
        this.routeList = routeList;
    }
}
