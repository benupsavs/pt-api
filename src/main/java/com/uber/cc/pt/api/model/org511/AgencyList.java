package com.uber.cc.pt.api.model.org511;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a list of 511 agencies.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AgencyList {

    @XmlElement(name = "Agency")
    private List<Agency> agencies;

    public List<Agency> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<Agency> agencies) {
        this.agencies = agencies;
    }
}
