package com.uber.cc.pt.api.model.org511;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a list of 511 transport routes.
 */
@XmlRootElement(name = "RouteList")
@XmlAccessorType(XmlAccessType.FIELD)
public class RouteList {

    private List<Route> routeList;

    public List<Route> getRouteList() {
        return routeList;
    }

    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
    }
}
