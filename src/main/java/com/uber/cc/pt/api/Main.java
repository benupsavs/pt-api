package com.uber.cc.pt.api;

import com.uber.cc.pt.api.client.BaseTransitClient;
import com.uber.cc.pt.api.client.TransitClient;
import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.model.Route;
import com.uber.cc.pt.api.model.Stop;
import com.uber.cc.pt.api.persistence.Persistence;
import com.uber.cc.pt.api.persistence.PersistenceEngine;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Main class, which starts an instance of the server.
 */
public class Main {

    /* Default base URI the Grizzly HTTP server will listen on, if none is provided. */
    public static final String DEFAULT_BASE_URI = "http://0.0.0.0:8080/pt/";

    /**
     * Starts a Grizzly HTTP server exposing JAX-RS resources defined in this
     * application.
     * @param baseUri the base URI to start the server on
     * @return Grizzly HTTP server
     * @see #DEFAULT_BASE_URI
     */
    public static HttpServer startServer(String baseUri) {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.uber.cc.pt.api package
        final ResourceConfig rc = new ResourceConfig().packages("com.uber.cc.pt.api.rs");
        rc.register(CORSResponseFilter.class);

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(baseUri), rc);
    }

    /**
     * Main method. If there are any arguments set, the first argument will
     * be used as the <em>base URI</em>, otherwise {@link #DEFAULT_BASE_URI}
     * will be used instead.
     * @param args the command line arguments
     * @throws IOException if an I/O error occurs while starting the server
     */
    public static void main(String[] args) throws IOException {
        final String baseUri = args.length > 0 ? args[0] : DEFAULT_BASE_URI;
        prime();
        final HttpServer server = startServer(baseUri);
        System.out.println(String.format("Jersey app started with WADL available at <base url>/application.wadl\nFor example: "
                + "%sapplication.wadl\nHit enter to stop it...", baseUri));
        System.in.read();
        server.shutdown(5, TimeUnit.SECONDS);
    }
    /**
     * Primes the local data store so that stop locations are available.
     */
    public static void prime() {
        final PersistenceEngine persistence = Persistence.getPersistenceEngine();
        final TransitClient transportClient = BaseTransitClient.getTransitClient();
        if (persistence.countAllStops() == 0) {
            System.out.println("Priming database. Please wait.");
            try {
                final List<Agency> agencies = transportClient.getAgencies();
                persistence.setAgencies(agencies);
                final CountDownLatch latch = new CountDownLatch(agencies.size());
                for (final Agency agency : agencies) {
                    // The remote service tends to disconnect when too many
                    // requests happen at the same time, so priming is done
                    // sequentially, with a gap between each request.
                    try {
                        final List<Route> detailedRoutes = transportClient.getDetailedRoutesForAgency(agency.getTag());
                        System.out.print(agency.getTitle() + "...");
                        int routes = 0;
                        int stops  = 0;
                        for (final Route detailedRoute : detailedRoutes) {
                            routes++;
                            for (final Stop stop : detailedRoute.getStops()) {
                                stops++;
                                stop.setAgencyTag(agency.getTag());
                                persistence.addStop(stop);
                            }
                            Thread.sleep(400);
                        }
                        System.out.println(routes + " routes, " + stops + " stops");
                    } catch (Exception ex) {
                        System.out.println("Problem: " + ex.getMessage());
                    } finally {
                        latch.countDown();
                    }
                }
                latch.await();
                System.out.println("Primed database with " + persistence.countAllStops() + " transit stops");
            } catch (Exception ex) {
                System.err.println("Unable to prime database");
                ex.printStackTrace();
            }
        }
    }
}

