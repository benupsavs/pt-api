package com.uber.cc.pt.api.persistence;

import java.util.ServiceLoader;

/**
 * Utility class to fetch the currently installed persistence engine.
 */
public class Persistence {

    private static volatile PersistenceEngine instance;

    // Prevent instantiation of utility class
    private Persistence() {}

    public static PersistenceEngine getPersistenceEngine() {
        // Efficient double checked locking implementation,
        // which only accesses the volatile field once under normal conditions
        PersistenceEngine pe = instance;
        if (pe == null) {
            synchronized (Persistence.class) {
                if (instance == null) {
                    for (PersistenceEngine e : ServiceLoader.load(PersistenceEngine.class)) {
                        instance = e;
                        break;
                    }
                }
                if (instance == null) {
                    // None found on classpath; fall back to in memory engine
                    instance = new MemoryPersistenceEngine();
                }

                pe = instance;
            }
        }

        return pe;
    }
}
