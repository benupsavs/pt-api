package com.uber.cc.pt.api.model.org511;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JAXB model representing a 511 transit route direction.
 */
@XmlRootElement(name = "RouteDirection")
@XmlAccessorType(XmlAccessType.FIELD)
public class RouteDirection {

    @XmlAttribute(name = "Code")
    private String code;

    @XmlAttribute(name = "Name")
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
