package com.uber.cc.pt.api.rs;

import com.uber.cc.pt.api.client.BaseTransitClient;
import com.uber.cc.pt.api.model.Agency;
import com.uber.cc.pt.api.client.TransitClient;
import com.uber.cc.pt.api.persistence.Persistence;
import com.uber.cc.pt.api.persistence.PersistenceEngine;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;

/**
 * REST service related to transport agency requests.
 */
@Path("agency")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AgencyService {

    private static final Logger LOG = Logger.getLogger(AgencyService.class.getName());

    private final PersistenceEngine persistence;
    private final TransitClient transportClient;

    public AgencyService() {
        persistence = Persistence.getPersistenceEngine();
        transportClient = BaseTransitClient.getTransitClient();
    }

    /**
     * GET method, fetching the list of available transport agencies.
     * @param asyncResponse the JAX-RS async response
     */
    @GET
    public void getAgencies(@Suspended final AsyncResponse asyncResponse) {
        final List<Agency> cachedAgencies = persistence.getAgencies();
        if (cachedAgencies == null) {
            new Thread("Fetch agencies") {

                @Override
                public void run() {
                    try {
                        final List<Agency> agencies = transportClient.getAgencies();
                        persistence.setAgencies(agencies);
                        asyncResponse.resume(new GenericEntity<List<Agency>>(agencies) {});
                    } catch (Exception ex) {
                        Logger.getLogger(AgencyService.class.getName()).log(Level.SEVERE, null, ex);
                        asyncResponse.resume(ex);
                    }
                }
            }.start();
        } else {
            asyncResponse.resume(new GenericEntity<List<Agency>>(cachedAgencies) {});
        }
    }
}
